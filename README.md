# Go言語のreflectの動きをみてみよう

## 以下試したこと

|関数|やってること|
|---|---|
|TypeCheck|interface2つの型の比較|
|IsModel|引数がModel型かをチェック(ポインタでもOK)|
|IsFieldExists|引数1に引数2のフィールドが存在するかをチェック(ポインタでもOK)|
|CheckFieldType|引数1のstructにある引数2のフィールドの型が引数3と一致する|
|GetValue|引数1のstructにある引数2のフィールドの値を返す|
|Peel|基本型の値を取得する|
|GetFieldList|構造体のフィールドの一覧を取得する|


## 試したいこと
* [x] 型比較
* [x] 型チェック
* [x] Modelのフィールドの存在チェック
* [x] Modelのフィールドの型チェック
* [x] Idの値の取得
* [x] Modelのフィールドの値の取得
* [x] フィールド一覧の配列の取得
* [ ] タグの一覧のマップの取得
