package golang_reflect

import (
	"reflect"
	"time"
)

type Id int

type Model struct {
	Id        Id        `form:"id"`
	Name      string    `form:"name"`
	IsDeleted bool      `form:"deleted"`
	CreatedAt time.Time `form:"created"`
}

func TypeCheck(foo, bar interface{}) bool {
	return reflect.TypeOf(foo) == reflect.TypeOf(bar)
}

func IsModel(foo interface{}) bool {
	if foo == nil {
		return false
	}

	return reflect.TypeOf(reflect.Indirect(reflect.ValueOf(foo)).Interface()) == reflect.TypeOf(Model{})
}

func IsFieldExists(i interface{}, fieldName string) bool {
	value := reflect.Indirect(reflect.ValueOf(i))

	if value.Kind() != reflect.Struct {
		return false
	}

	field := value.FieldByName(fieldName)
	return field.IsValid()
}

func CheckFieldType(i interface{}, fieldName string, t interface{}) bool {
	if !IsFieldExists(i, fieldName) {
		return false
	}

	value := reflect.Indirect(reflect.ValueOf(i))
	field := value.FieldByName(fieldName)

	return field.Type() == reflect.TypeOf(t)
}

func GetValue(i interface{}, fieldName string) interface{} {
	if !IsFieldExists(i, fieldName) {
		return nil
	}

	value := reflect.Indirect(reflect.ValueOf(i))
	field := value.FieldByName(fieldName)

	return field.Interface()
}

func Peel(i interface{}) interface{} {
	value := reflect.Indirect(reflect.ValueOf(i))
	switch value.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return value.Int()
	case reflect.String:
		return value.String()
	case reflect.Bool:
		return value.Bool()
	default:
		return nil
	}
	return nil
}

func GetFieldList(i interface{}) (list []string) {
	list = []string{}

	value := reflect.Indirect(reflect.ValueOf(i))

	if value.Kind() != reflect.Struct {
		return
	}

	for index := 0; index < value.NumField(); index++ {
		list = append(list, value.Type().Field(index).Name)
	}

	return
}

func GetFieldTagList(i interface{}) (list map[string]string) {
	list = map[string]string{}

	value := reflect.Indirect(reflect.ValueOf(i))

	if value.Kind() != reflect.Struct {
		return
	}

	for index := 0; index < value.NumField(); index++ {
		fieldName := value.Type().Field(index).Name
		tag := string(value.Type().Field(index).Tag)
		list[fieldName] = tag
	}

	return
}
