package golang_reflect

import (
	"reflect"
	"testing"
)

func fatal(t *testing.T, index int, expect interface{}, actual interface{}) {
	t.Fatalf("%s No.%dで失敗。 期待: %+v(%+v), 実際: %+v(%+v)",
		t.Name(), index+1, expect, reflect.TypeOf(expect), actual, reflect.TypeOf(actual))
}

func pass(t *testing.T, index int) {
	t.Logf("%s No.%d 成功", t.Name(), index+1)
}

func TestTypeCheck(t *testing.T) {
	type params struct {
		foo interface{}
		bar interface{}
	}
	testTable := []struct {
		params params
		expect bool
	}{
		{params{Id(1), Id(2)}, true},
		{params{Id(1), new(Id)}, false},
		{params{Id(1), nil}, false},
		{params{new(Id), new(Id)}, true},
		{params{new(Id), nil}, false},
		{params{Model{}, Model{Id: Id(1)}}, true},
		{params{Model{}, new(Model)}, false},
		{params{Model{}, nil}, false},
		{params{new(Model), new(Model)}, true},
		{params{new(Model), nil}, false},
	}

	for i, test := range testTable {
		actual := TypeCheck(test.params.foo, test.params.bar)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestIsModel(t *testing.T) {
	testTable := []struct {
		param  interface{}
		expect bool
	}{
		{Model{}, true},
		{Id(1), false},
		{"Model{}", false},
		{new(Model), true},
		{new(Id), false},
		{new(string), false},
		{nil, false},
	}

	for i, test := range testTable {
		actual := IsModel(test.param)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestIsFieldExists(t *testing.T) {
	type params struct {
		model     interface{}
		fieldName string
	}
	testTable := []struct {
		params params
		expect bool
	}{
		{params{Model{}, "Id"}, true},
		{params{Model{}, "id"}, false},
		{params{new(Model), "Id"}, true},
		{params{new(Model), "id"}, false},
		{params{nil, "Id"}, false},
		{params{Id(1), "Id"}, false},
		{params{new(Id), "Id"}, false},
	}

	for i, test := range testTable {
		actual := IsFieldExists(test.params.model, test.params.fieldName)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestCheckFieldType(t *testing.T) {
	type params struct {
		model     interface{}
		fieldName string
		t         interface{}
	}
	testTable := []struct {
		params params
		expect bool
	}{
		{params{Model{}, "Id", Id(1)}, true},
		{params{Model{}, "Id", new(Id)}, false},
		{params{Model{}, "Name", ""}, true},
		{params{Model{}, "Name", false}, false},
		{params{Model{}, "IsDeleted", false}, true},
		{params{new(Model), "Id", Id(1)}, true},
		{params{new(Model), "Id", new(Id)}, false},
	}

	for i, test := range testTable {
		actual := CheckFieldType(test.params.model, test.params.fieldName, test.params.t)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestGetValue(t *testing.T) {
	type params struct {
		model     interface{}
		fieldName string
	}
	testTable := []struct {
		params params
		expect interface{}
	}{
		{params{Model{Id: Id(1)}, "Id"}, Id(1)},
		{params{Model{}, "Id"}, Id(0)},
		{params{Model{}, "Name"}, ""},
		{params{new(Model), "Id"}, Id(0)},
		{params{Model{}, "id"}, nil},
		{params{new(Model), "id"}, nil},
	}

	for i, test := range testTable {
		actual := GetValue(test.params.model, test.params.fieldName)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestPeel(t *testing.T) {
	testTable := []struct {
		param  interface{}
		expect interface{}
	}{
		{Id(1), int64(1)},
		{new(Id), int64(0)},
		{Model{}, nil},
		{1, int64(1)},
		{"", ""},
		{false, false},
	}

	for i, test := range testTable {
		actual := Peel(test.param)
		if test.expect != actual {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestGetFieldList(t *testing.T) {
	testTable := []struct {
		param  interface{}
		expect []string
	}{
		{Model{}, []string{"Id", "Name", "IsDeleted", "CreatedAt"}},
		{new(Model), []string{"Id", "Name", "IsDeleted", "CreatedAt"}},
		{Id(0), []string{}},
		{struct{ Foo, Bar, Baz string }{}, []string{"Foo", "Bar", "Baz"}},
		{struct{ foo, bar, baz string }{}, []string{"foo", "bar", "baz"}},
	}

	for i, test := range testTable {
		actual := GetFieldList(test.param)
		if !reflect.DeepEqual(test.expect, actual) {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}

func TestGetFieldTagList(t *testing.T) {
	modelExpect := map[string]string{
		"Id":        `form:"id"`,
		"Name":      `form:"name"`,
		"IsDeleted": `form:"deleted"`,
		"CreatedAt": `form:"created"`,
	}

	testTable := []struct {
		param  interface{}
		expect interface{}
	}{
		{Model{}, modelExpect},
		{new(Model), modelExpect},
		{Id(1), map[string]string{}},
		{0, map[string]string{}},
	}

	for i, test := range testTable {
		actual := GetFieldTagList(test.param)
		if !reflect.DeepEqual(test.expect, actual) {
			fatal(t, i, test.expect, actual)
		}

		pass(t, i)
	}
}
